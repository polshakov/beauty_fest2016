<?
if(!isset($_GET["dev"]))
{
	header('Location: http://www.podrygka.ru');
	exit;
}
?>

<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ru" class="lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html lang="ru" class="lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html lang="ru" class="lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="ru">
<!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<title>ВЫИГРЫВАЙ КАЖДЫЙ ДЕНЬ ОДИН ИЗ 5 ГОДОВЫХ НАБОРОВ СЕРТИФИКАТОВ В «ПОДРУЖКУ»! | Скидки и акции | Сеть магазинов «Подружка»</title>
	<meta name="description" content="ВЫИГРЫВАЙ КАЖДЫЙ ДЕНЬ ОДИН ИЗ 5 ГОДОВЫХ НАБОРОВ СЕРТИФИКАТОВ В «ПОДРУЖКУ»! | и другие выгодные предложения в магазинах «Подружка». Здесь вы можете купить декоративную и уходовую косметику и парфюмерию по привлекательным ценам. Широкий ассортимент продукции известных брендов, скидки и акции." />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="shortcut icon" href="favicon.png" />

	<script src="http://api-maps.yandex.ru/2.0-stable/?load=package.full&lang=ru-RU"></script>

	<link rel="stylesheet" href="css/reset.css" />
	<link rel="stylesheet" href="libs/Swiper/dist/css/swiper.min.css"/>
	<link rel="stylesheet" href="libs/font-awesome-4.2.0/css/font-awesome.min.css" />
	<link rel="stylesheet" href="libs/fancybox/jquery.fancybox.css" />
	<link rel="stylesheet" href="libs/countdown/jquery.countdown.css">
	<link rel="stylesheet" href="css/fonts.css" />
	<link rel="stylesheet" href="css/main.css" />
	<link rel="stylesheet" href="css/media.css" />
</head>
<body id="skrollr-body">
	<header>
		<div class="header__big-sun_container">
			<img src="img/sun_desktop.png" alt="" class="header__big-sun"/>
		</div>

		<div class="header__main-content_container">
			<div class="main-content__callcenter">
				<span>Call-центр</span>
				<a href="tel:88007074747">8 800 707 47 47</a>
			</div>

			<div class="main-content__promo-text">
				<p>
					<span class="bold">
						<span class="light-blue">Выигрывай каждый день один из </span>
						<span class="pink"><span class="font-weight-bold">5</span> годовых наборов сертификатов<br/>в «Подружку»!</span>
						<br/>
					</span>
					<span class="pink sm-hidden">С 1 по 30 сентября 2016 года</span>
				</p>
			</div>

			<div class="main-content__gifts-blocks">
				<img src="img/gift_sertificates.png" alt="" class="main-content__gift-sertificates" />
				<img src="img/garant_gifts.png" alt="" class="main-content__garant-gifts" />
			</div>

			<div class="main-content__girls">
				<img src="img/girls.png" alt=""/>
			</div>
			<div class="main-content__yellow-circle-lines parallax-style skrollable skrollable-between" data-0="background-position:0px 400px;" data-100000="background-position:0px -50000px;"></div>
			<div class="main-content__pink-dotted-circle parallax-style skrollable skrollable-between" data-0="background-position:0px 450px;" data-100000="background-position:0px -50000px;"></div>

			<div class="header__bottom">
				<div class="header__brushes">
					<img src="img/brushes.png" alt=""/>
				</div>

				<div class="header__bottom-line">

					<div class="header__countdown">
						<div class="header__countdown-title">
							<span>До выбора победителя</span>
						</div>
						<div id="counter"></div>
						<div class="count-defines">
							<div>Часов</div>
							<div>Минут</div>
							<div>Секунд</div>
						</div>
					</div>
					<div class="header__gift-cards">
						<img src="img/sertif_cards.png" alt=""/>
					</div>
					<div class="header__podrygka-logo">
						<a href="http://www.podrygka.ru/"><img src="img/podrygka-logo.png" alt=""/></a>
					</div>
					<img src="img/print.png" alt="" class="main-content__print_img"/>
				</div>
			</div>

                <img src="img/decor/header/1.png" alt="" class="decors decor-1"><img src="img/decor/header/2.png" alt=""
                                                                              class="decors decor-2"><img
                    src="img/decor/header/3.png" alt=""
                    class="decors decor-3"><img
                    src="img/decor/header/4.png" alt="" class="decors decor-4"><img src="img/decor/header/5.png" alt=""
                                                                             class="decors decor-5"><img
                    src="img/decor/header/6.png" alt=""
                    class="decors decor-6"><img
                    src="img/decor/header/7.png" alt="" class="decors decor-7"><img src="img/decor/header/8.png" alt=""
                                                                             class="decors decor-8"><img
                    src="img/decor/header/9.png" alt=""
                    class="decors decor-9"><img
                    src="img/decor/header/10.png" alt="" class="decors decor-10">

		</div>
	</header>

	<main>
		<div class="partners">
			<div class="blue_circle_parallax parallax-style skrollable skrollable-between"  data-0="background-position:0px 400px;" data-100000="background-position:0px -50000px;"></div>
			<div class="yellow_dotted_circle_parallax parallax-style skrollable skrollable-between" data-0="background-position:0px 800px;" data-100000="background-position:0px -50000px;"></div>

			<div class="container less centred">

				<div class="partners__title">
					<h2 class="main-pink">Партнеры</h2>
				</div>

				<div class="partners__partners-list">
					<!--<img src="img/partners.jpg" alt=""/>-->
					<ul>
						<li><a href="http://www.podrygka.ru/catalog/?arrFilter_158_2063387875=Y&arrFilter_P1_MIN=9&arrFilter_P1_MAX=7560&set_filter=Y"><img src="img/partners/pinkup.png" alt=""/></a></li>
						<li><a href="http://www.podrygka.ru/catalog/?arrFilter_158_2836788418=Y&arrFilter_P1_MIN=9&arrFilter_P1_MAX=7560&set_filter=Y"><img src="img/partners/bourjios.png" alt=""/></a></li>
						<li><a href="http://www.podrygka.ru/catalog/?arrFilter_158_742949564=Y&arrFilter_P1_MIN=9&arrFilter_P1_MAX=7560&set_filter=Y"><img src="img/partners/loreal.png" alt=""/></a></li>
						<li><a href="http://www.podrygka.ru/catalog/?arrFilter_158_774376630=Y&arrFilter_P1_MIN=9&arrFilter_P1_MAX=7560&set_filter=Y"><img src="img/partners/dove.png" alt=""/></a></li>
						<li><a href="http://www.podrygka.ru/catalog/?arrFilter_158_3111145506=Y&arrFilter_P1_MIN=9&arrFilter_P1_MAX=7560&set_filter=Y"><img src="img/partners/schwarzkopf.png" alt=""/></a></li>
						<li><a href="http://www.podrygka.ru/catalog/?arrFilter_158_2699979093=Y&arrFilter_P1_MIN=9&arrFilter_P1_MAX=7560&set_filter=Y"><img src="img/partners/deco.png" alt=""/></a></li>
						<li><a href="http://www.podrygka.ru/catalog/?arrFilter_158_2897480606=Y&arrFilter_P1_MIN=9&arrFilter_P1_MAX=7560&set_filter=Y"><img src="img/partners/orly.png" alt=""/></a></li>
						<li><a href="http://www.podrygka.ru/catalog/?arrFilter_158_3478905690=Y&arrFilter_P1_MIN=9&arrFilter_P1_MAX=7560&set_filter=Y"><img src="img/partners/ladypink.png" alt=""/></a></li>
						<li><a href="http://www.podrygka.ru/catalog/?arrFilter_158_1358765262=Y&arrFilter_P1_MIN=9&arrFilter_P1_MAX=7560&set_filter=Y"><img src="img/partners/maxfactor.png" alt=""/></a></li>
						<li><a href="http://www.podrygka.ru/catalog/?arrFilter_158_3396376858=Y&arrFilter_P1_MIN=9&arrFilter_P1_MAX=7560&set_filter=Y"><img src="img/partners/himalaya.png" alt=""/></a></li>
						<li><a href="http://www.podrygka.ru/catalog/?arrFilter_158_1606517811=Y&arrFilter_P1_MIN=9&arrFilter_P1_MAX=7560&set_filter=Y"><img src="img/partners/artdeco.png" alt=""/></a></li>
						<li><a href="http://www.podrygka.ru/catalog/?arrFilter_158_834315146=Y&arrFilter_P1_MIN=9&arrFilter_P1_MAX=7560&set_filter=Y"><img src="img/partners/garnier.png" alt=""/></a></li>
						<li><a href="http://www.podrygka.ru/catalog/?arrFilter_158_232249209=Y&arrFilter_P1_MIN=9&arrFilter_P1_MAX=7560&set_filter=Y"><img src="img/partners/blackpearl.png" alt=""/></a></li>
						<li><a href="http://www.podrygka.ru/catalog/?arrFilter_158_546826305=Y&arrFilter_P1_MIN=9&arrFilter_P1_MAX=7560&set_filter=Y"><img src="img/partners/maybelline.png" alt=""/></a></li>
						<li><a href="http://www.podrygka.ru/catalog/?arrFilter_158_143841955=Y&arrFilter_P1_MIN=9&arrFilter_P1_MAX=7560&set_filter=Y"><img src="img/partners/sallyhansen.png" alt=""/></a></li>
						<li><a href="http://www.podrygka.ru/catalog/?arrFilter_158_2552534726=Y&arrFilter_P1_MIN=9&arrFilter_P1_MAX=7560&set_filter=Y"><img src="img/partners/nivea.png" alt=""/></a></li>
						<li><a href="http://www.podrygka.ru/catalog/?arrFilter_158_94419918=Y&arrFilter_P1_MIN=9&arrFilter_P1_MAX=7560&set_filter=Y"><img src="img/partners/catrice.png" alt=""/></a></li>
						<li><a href="http://www.podrygka.ru/catalog/?arrFilter_158_2640890600=Y&arrFilter_P1_MIN=9&arrFilter_P1_MAX=7560&set_filter=Y"><img src="img/partners/cleanline.png" alt=""/></a></li>
						<li><a href="http://www.podrygka.ru/catalog/?arrFilter_158_1229076393=Y&arrFilter_P1_MIN=9&arrFilter_P1_MAX=7560&set_filter=Y"><img src="img/partners/syoss.png" alt=""/></a></li>
						<li><a href="http://www.podrygka.ru/catalog/?arrFilter_158_2794339506=Y&arrFilter_P1_MIN=9&arrFilter_P1_MAX=7560&set_filter=Y"><img src="img/partners/vivienne.png" alt=""/></a></li>
						<li><a href="http://www.podrygka.ru/catalog/?arrFilter_158_2182653050=Y&arrFilter_P1_MIN=9&arrFilter_P1_MAX=7560&set_filter=Y"><img src="img/partners/nyx.png" alt=""/></a></li>
						<li><a href="http://www.podrygka.ru/catalog/?arrFilter_158_1888225328=Y&arrFilter_P1_MIN=9&arrFilter_P1_MAX=7560&set_filter=Y"><img src="img/partners/pantene.png" alt=""/></a></li>
						<li><a href="http://www.podrygka.ru/catalog/?arrFilter_158_238568133=Y&arrFilter_P1_MIN=9&arrFilter_P1_MAX=7560&set_filter=Y"><img src="img/partners/fa.png" alt=""/></a></li>
						<li><a href="http://www.podrygka.ru/catalog/?arrFilter_158_3250731006=Y&arrFilter_P1_MIN=9&arrFilter_P1_MAX=7560&set_filter=Y"><img src="img/partners/gillette.png" alt=""/></a></li>
						<li><a href="http://www.podrygka.ru/catalog/?arrFilter_158_2716072107=Y&arrFilter_P1_MIN=9&arrFilter_P1_MAX=7560&set_filter=Y"><img src="img/partners/wellaflex.png" alt=""/></a></li>
						<li><a href="http://www.podrygka.ru/catalog/?arrFilter_158_1573218044=Y&arrFilter_P1_MIN=9&arrFilter_P1_MAX=7560&set_filter=Y"><img src="img/partners/aussie.png" alt=""/></a></li>
					</ul>
				</div>

				<div class="partners__info-parnter_container">
					<h3 class="main-pink">Информационный партнер</h3>
					<div class="partners__woman-parnter">
						<a href="#">
							<img src="img/woman_ru.jpg" alt=""/>
						</a>
					</div>
				</div>
			</div>

                <img src="img/decor/partners/1.png" alt="" class="decors decor-1"><img src="img/decor/partners/2.png" alt=""
                                                                              class="decors decor-2"><img
                    src="img/decor/partners/3.png" alt=""
                    class="decors decor-3"><img
                    src="img/decor/partners/4.png" alt="" class="decors decor-4"><img src="img/decor/partners/5.png" alt=""
                                                                             class="decors decor-5"><img
                    src="img/decor/partners/6.png" alt=""
                    class="decors decor-6">
  
		</div>

		<div class="garant-presents">
			<div class="orange_circle_parallax parallax-style skrollable skrollable-between" data-0="background-position:0px 1000px;" data-100000="background-position:0px -50000px;"></div>
			<div class="garant-presents__title">
				<h2>Гарантированные подарки</h2>
			</div>
			<div class="container more centred">
				<div class="garant-presents__content">
					<div class="garant-presents__presents">
						<img src="img/garant_presents_light.png" alt=""/>
						<img src="img/percent.png" alt="" class="percent_pict"/>
					</div>

					<div class="garant-presents__presents-defines">
						<div class="umbrella">
							<span>Зонт «Подружка»</span><img src="img/umbrella_line_large.png" alt=""/>
						</div>

						<div class="kosmetichka">
							<span>Косметичка «Подружка»</span><img src="img/kosmetichka_line_large.png" alt=""/>
						</div>

						<div class="povyazka">
							<span>Подвязка «Подружка»</span><img src="img/povyazka_line_large.png" alt=""/>
						</div>

						<div class="brush">
							<span>Кисть «Подружка»</span><img src="img/brush_line_large.png" alt=""/>
						</div>

						<div class="hairbrush">
							<span>Расческа «Подружка»</span><img src="img/hairbrush_line_large.png" alt=""/>
						</div>
						<div class="percent">
							<span>Скидка<br/>на любимые бренды</span><img src="img/percent_line_large.png" alt=""/>
						</div>
					</div>
				</div>
			</div>

			<div class="garant__presents-slider">
				<div class="swiper-container">

					<div class="swiper-wrapper">

						<div class="swiper-slide">
							<div class="picture">
								<img src="img/garant-slider/umbrella.png" alt=""/>
							</div>
							<div class="title">
								<span>Зонт «Подружка»</span>
							</div>
						</div>
						<div class="swiper-slide">
							<div class="picture">
								<img src="img/garant-slider/kosmetichka.png" alt=""/>
							</div>
							<div class="title">
								<span>Косметичка «Подружка»</span>
							</div>
						</div>
						<div class="swiper-slide">
							<div class="picture">
								<img src="img/garant-slider/brush.png" alt=""/>
							</div>
							<div class="title">
								<span>Кисть «Подружка»</span>
							</div>
						</div>
						<div class="swiper-slide">
							<div class="picture">
								<img src="img/garant-slider/hairbrush.png" alt=""/>
							</div>
							<div class="title">
								<span>Расческа «Подружка»</span>
							</div>
						</div>
						<div class="swiper-slide">
							<div class="picture">
								<img style="max-width: none; max-height: none; width: 100%;" src="img/garant-slider/povyazka.png" alt=""/>
							</div>
							<div class="title">
								<span>Повязка «Подружка»</span>
							</div>
						</div>
						<div class="swiper-slide">
							<div class="picture">
								<img src="img/garant-slider/percent.png" alt=""/>
							</div>
							<div class="title">
								<span>Скидка на любимые бренды</span>
							</div>
						</div>

					</div>

					<div class="swiper-button-prev"></div>
					<div class="swiper-button-next"></div>
				</div>
			</div>
                <img src="img/decor/garant/1.png" alt="" class="decors decor-1">
                <img src="img/decor/garant/2.png" alt="" class="decors decor-2">
                <img src="img/decor/garant/3.png" alt="" class="decors decor-3">
                <img src="img/decor/garant/4.png" alt="" class="decors decor-4">
                <img src="img/decor/garant/5.png" alt="" class="decors decor-5">
                <img src="img/decor/garant/6.png" alt="" class="decors decor-6">
                <img src="img/decor/garant/7.png" alt="" class="decors decor-7">
                <img src="img/decor/garant/8.png" alt="" class="decors decor-8">
		</div>

		<div class="calendar">
			<div class="calendar__block-decorations container more">
				<!-- Кисточки, помады, карандаши и прочая ересь.-->
				<div class="left-decor-notepad-points">
					<div class="point"></div>
					<div class="point"></div>
				</div>
				<div class="right-decor-notepad-points">
					<div class="point"></div>
					<div class="point"></div>
				</div>

				<div class="left-big-decors">
                    <div class="pencil"></div>
                    <div class="pomada"></div>
                </div>

                <div class="right-big-decors">
                    <div class="circles"></div>
                    <div class="brush"></div>
                    <div class="little-brush"></div>
                    <div class="tush"></div>
                </div>
			</div>
			<div class="calendar__top-part">
				<div class="container less centred">
					<div class="calendar__title">
						<h2>Календарь красоты</h2>
					</div>
					<div class="calendar__description">
						<p>
							Не пропусти дни красоты в "Подружке"! В эти даты, отмеченные в календаре розовым цветом,
							ты сможешь сделать экспресс-макияж от визажистов ведущих брендов декоративной косметики!<br/>
							А также получить подарки и консультацию профессионалов!
						</p>
					</div>
					<div class="calendar__some-parnters">
						<ul>
							<li><a href="http://www.podrygka.ru/catalog/?arrFilter_158_1358765262=Y&arrFilter_P1_MIN=9&arrFilter_P1_MAX=7560&set_filter=Y"><img class="maxfactor" src="img/max-factor.png" alt=""/></a></li>
							<li><a href="http://www.podrygka.ru/catalog/?arrFilter_158_519910614=Y&arrFilter_P1_MIN=9&arrFilter_P1_MAX=7560&set_filter=Y"><img class="divage" src="img/divage.png" alt=""/></a></li>
							<li><a href="http://www.podrygka.ru/catalog/?arrFilter_158_2794339506=Y&arrFilter_P1_MIN=9&arrFilter_P1_MAX=7560&set_filter=Y"><img class="vivenne_sabo" src="img/vivenne_sabo.png" alt=""/></a></li>
							<li><a href="http://www.podrygka.ru/catalog/?arrFilter_158_1606517811=Y&arrFilter_P1_MIN=9&arrFilter_P1_MAX=7560&set_filter=Y"><img class="artdeco" src="img/artdeco.png" alt=""/></a></li>
							<li><a href="http://www.podrygka.ru/catalog/?arrFilter_158_2897480606=Y&arrFilter_P1_MIN=9&arrFilter_P1_MAX=7560&set_filter=Y"><img src="img/partners/orly.png" style="width:70%" alt=""></a></li>
							<li><a href="http://www.podrygka.ru/catalog/?arrFilter_158_1492320267=Y&arrFilter_P1_MIN=9&arrFilter_P1_MAX=7560&set_filter=Y"><img src="img/modal-sleek.png" style="width:55%" alt=""></a></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="calendar__bottom-part">
				<div class="container less centred">
					<div class="calendar__month">
						<span>Сентябрь</span>
					</div>

					<div class="calendar__table-container">
						<table>
							<thead>
								<tr>
									<th>пн</th>
									<th>вт</th>
									<th>ср</th>
									<th>чт</th>
									<th>пт</th>
									<th>сб</th>
									<th>вс</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="orange"><span></span><span></span></td>
									<td class="orange"><span></span><span></span></td>
									<td class="orange"><span></span><span></span></td>
									<td><span></span><span></span></td>
									<td class="event-pink">
										<a href="#" class="modal-win modal-event" data-modal="#simple-event">
											<span>2</span><span>сентября</span><span class="mobile">сен.</span>
										</a>
									</td>
									<td><span></span><span></span></td>
									<td><span></span><span></span></td>
								</tr>
								<tr>
									<td><span></span><span></span></td>
									<td><span></span><span></span></td>
									<td><span></span><span></span></td>
									<td class="event-pink">
										<a href="#" class="modal-win modal-event" data-modal="#event1"><span>8</span><span>сентября</span><span class="mobile">сен.</span>
										</a>
									</td>
									<td class="event-pink">
										<a href="#" class="modal-win modal-event" data-modal="#event2">
											<span>9</span><span>сентября</span><span class="mobile">сен.</span>
										</a>
									</td>
									<td class="event-pink">
										<a href="#" class="modal-win modal-event" data-modal="#event3">
											<span>10</span><span>сентября</span><span class="mobile">сен.</span>
										</a>
									</td>
									<td><span></span><span></span></td>
								</tr>
								<tr>
									<td><span></span><span></span></td>
									<td><span></span><span></span></td>
									<td><span></span><span></span></td>
									<td class="event-pink">
										<a href="#" class="modal-win modal-event" data-modal="#event4">
											<span>15</span><span>сентября</span><span class="mobile">сен.</span>
										</a>
									</td>
									<td><span></span><span></span></td>
									<td class="event-pink">
										<a href="#" class="modal-win modal-event" data-modal="#event5">
											<span>17</span><span>сентября</span><span class="mobile">сен.</span>
										</a>
									</td>
									<td><span></span><span></span></td>
								</tr>
								<tr>
									<td><span></span><span></span></td>
									<td><span></span><span></span></td>
									<td><span></span><span></span></td>
									<td class="event-pink">
										<a href="#" class="modal-win modal-event" data-modal="#event6">
											<span>22</span><span>сентября</span><span class="mobile">сен.</span>
										</a>
									</td>
									<td class="event-pink">
										<a href="#" class="modal-win modal-event" data-modal="#event7">
											<span>23</span><span>сентября</span><span class="mobile">сен.</span>
										</a>
									</td>
									<td class="event-pink">
										<a href="#" class="modal-win modal-event" data-modal="#event8">
											<span>24</span><span>сентября</span><span class="mobile">сен.</span>
										</a>
									</td>
									<td><span></span><span></span></td>
								</tr>
								<tr>
									<td><span></span><span></span></td>
									<td><span></span><span></span></td>
									<td><span></span><span></span></td>
									<td class="event-pink">
										<a href="#" class="modal-win modal-event" data-modal="#event9">
											<span>29</span><span>сентября</span><span class="mobile">сен.</span>
										</a>
									</td>
									<td class="event-pink">
										<a href="#" class="modal-win modal-event" data-modal="#event10">
											<span>30</span><span>сентября</span><span class="mobile">сен.</span>
										</a>
									</td>
									<td class="orange"><span></span><span></span></td>
									<td class="orange"><span></span><span></span></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="calendar__paper-corner"></div> <!-- Загнутый листочек -->
				<div class="flower_circle_parallax parallax-style skrollable skrollable-between" data-0="background-position:0px 2200px;" data-100000="background-position:0px -50000px;"></div>
			</div>
            
                <img src="img/decor/calendar/1.png" alt="" class="decors decor-1">
                <img src="img/decor/calendar/2.png" alt="" class="decors decor-2">
                <img src="img/decor/calendar/3.png" alt="" class="decors decor-3">
                <img src="img/decor/calendar/4.png" alt="" class="decors decor-4">
                <img src="img/decor/calendar/5.png" alt="" class="decors decor-5">
                <img src="img/decor/calendar/6.png" alt="" class="decors decor-6">
                <img src="img/decor/calendar/7.png" alt="" class="decors decor-7">
                <img src="img/decor/calendar/8.png" alt="" class="decors decor-8">
                <img src="img/decor/calendar/9.png" alt="" class="decors decor-9">
                <img src="img/decor/calendar/10.png" alt="" class="decors decor-10">
		</div>

		<div id="rules" class="rules">
			<div class="green_circle_parallax parallax-style skrollable skrollable-between" data-0="background-position:0px 2600px;" data-100000="background-position:0px -50000px;"></div>
			<div class="yellow_circle_lines_parallax parallax-style skrollable skrollable-between" data-0="background-position:0px 3000px;" data-100000="background-position:0px -50000px;"></div>

			<div class="container more centred">
				<div class="rules__title">
					<h2 class="main-pink">Правила участия</h2>
				</div>

				<div class="rules__the-way">
					<div>
						<img class="desktop" src="img/the_way1250.png" alt=""/>
						<img class="mobile" src="img/the_way320.png" alt=""/>
					</div>

					<div class="rules__steps">
						<div id="mouse-parallax1" class="rules__step1">
							<div class="picture"><img src="img/step_1.png" alt=""/></div>
							<div class="description">
								<div><span>Шаг <span class="digit">1</span>:</span></div>
								<p>Приходи в магазин "Подружка"<br/>с <strong class="digitTahoma">1</strong> сентября по <strong class="digitTahoma">30</strong> сентября</p>
							</div>
						</div>
						<div id="mouse-parallax2" class="rules__step2">
							<div class="picture"><img src="img/step_2.png" alt=""/></div>
							<div class="description">
								<div><span>Шаг <span class="digit">2</span>:</span></div>
								<p>Соверши покупку от <strong class="digitTahoma">750</strong> рублей</p>
							</div>
						</div>
						<div id="mouse-parallax3" class="rules__step3">
							<div class="picture"><img src="img/step_3.png" alt=""/></div>
							<div class="description">
								<div><span>Шаг <span class="digit">3</span>:</span></div>
								<p>Получи<br/>скретч-карту</p>
							</div>
						</div>
						<div id="mouse-parallax4" class="rules__step4">
							<div class="picture"><img src="img/step_4.png" alt=""/></div>
							<div class="description">
								<div><span>Шаг <span class="digit">4</span>:</span></div>
								<p>Сотри код и узнай, какой гарантированный подарок ты получишь</p>
							</div>
						</div>
						<div id="mouse-parallax5" class="rules__step5">
							<div class="picture"><img src="img/step_5.png" alt=""/></div>
							<div class="description">
								<div><span>Шаг <span class="digit">5</span>:</span></div>
								<p>Если ты владелец нашей карты "Подружка", <br/>у тебя есть возможность выиграть главный приз</p>
							</div>
						</div>
					</div>
				</div>
			</div>
            
                <img src="img/decor/rules/1.png" alt="" class="decors decor-1">
                <img src="img/decor/rules/2.png" alt="" class="decors decor-2">
                <img src="img/decor/rules/3.png" alt="" class="decors decor-3">
                <img src="img/decor/rules/4.png" alt="" class="decors decor-4">
                <img src="img/decor/rules/5.png" alt="" class="decors decor-5">
                <img src="img/decor/rules/6.png" alt="" class="decors decor-6">
		</div>

		<div class="instagram">
			<div class="yellow_dotted_circle_parallax parallax-style skrollable skrollable-between" data-0="background-position:0px 3000px;" data-100000="background-position:0px -50000px;"></div>

			<div class="container less centred">
				<div class="instagram__title">
					<h2 class="main-pink">
						Конкурс в Instagram
					</h2>
				</div>

				<div class="instagram__description">
					<h3>#Фестиваль_Красоты_Подружка</h3>

					<p>
						<strong>Создай яркий образ в осеннем стиле с косметикой из "Подружки"!</strong><br/>
						Выложи фото своего образа в Instagram  и поставь хэштег #Фестиваль_Красоты_Подружка.
						Лучшие beauty-образы определит жюри конкурса, а их авторы получат подарочные сертификаты в магазин "Подружка"!<br/>
						Не забудь подписаться @podrygkashop!
					</p>
				</div>

				<div class="instagram__sliders">
					<div id="slider-1" class="instagram__slider">
						<div class="swiper-container">
							<div class="swiper-wrapper">
								<div class="slider-item swiper-slide"><a href="#"><img src="img/slider/1_299x257.jpg" alt=""/></a>
								</div>

								<div class="slider-item swiper-slide"><a href="#"><img src="img/slider/6_299x257.jpg" alt=""/></a>
								</div>

								<div class="slider-item swiper-slide"><a href="#"><img src="img/slider/3_299x257.jpg" alt=""/></a>
								</div>
							</div>
						</div>
						<div class="swiper-button-prev"></div>
						<div class="swiper-button-next"></div>
					</div>
					<div id="slider-2" class="instagram__slider">
						<div class="swiper-container">
							<div class="swiper-wrapper">
								<div class="slider-item swiper-slide"><a href="#"><img src="img/slider/2_299x257.jpg" alt=""/></a>
								</div>

								<div class="slider-item swiper-slide"><a href="#"><img src="img/slider/4_299x257.jpg" alt=""/></a>
								</div>

								<div class="slider-item swiper-slide"><a href="#"><img src="img/slider/5_299x257.jpg" alt=""/></a>
								</div>
							</div>
						</div>
						<div class="swiper-button-prev"></div>
						<div class="swiper-button-next"></div>
					</div>
					<div id="slider-3" class="instagram__slider">
						<div class="swiper-container">
							<div class="swiper-wrapper">
								<div class="slider-item swiper-slide"><a href="#"><img src="img/slider/3_299x257.jpg" alt=""/></a>
								</div>

								<div class="slider-item swiper-slide"><a href="#"><img src="img/slider/1_299x257.jpg" alt=""/></a>
								</div>

								<div class="slider-item swiper-slide"><a href="#"><img src="img/slider/2_299x257.jpg" alt=""/></a>
								</div>
							</div>
						</div>
						<div class="swiper-button-prev"></div>
						<div class="swiper-button-next"></div>
					</div>
				</div>
			</div>

            
                <img src="img/decor/inst/1.png" alt="" class="decors decor-1">
                <img src="img/decor/inst/2.png" alt="" class="decors decor-2">
                <img src="img/decor/inst/3.png" alt="" class="decors decor-3">
                <img src="img/decor/inst/4.png" alt="" class="decors decor-4">
                <img src="img/decor/inst/5.png" alt="" class="decors decor-5">
                <img src="img/decor/inst/6.png" alt="" class="decors decor-6">
                <img src="img/decor/inst/7.png" alt="" class="decors decor-7">
		</div>

		<div class="winners">
			<div class="green_dashed_circle_parallax parallax-style skrollable skrollable-between" data-0="background-position:0px 3600px;" data-100000="background-position:0px -50000px;"></div>

			<div class="container more centred">
				<div class="winners__title">
					<h2 class="main-pink">
						Победители
					</h2>
				</div>

				<div class="winners__calendar_container">
					<div class="winners__calendar-date">
						<div class="day font-weight-bold">
							<ul>
								<li><span>1</span></li>
								<li><span>2</span></li>
								<li><span>3</span></li>
								<li><span>4</span></li>
								<li><span>5</span></li>
								<li><span>6</span></li>
								<li><span>7</span></li>
								<li><span>8</span></li>
								<li><span>9</span></li>
								<li><span>10</span></li>
								<li><span>11</span></li>
								<li><span>12</span></li>
								<li><span>13</span></li>
								<li><span>14</span></li>
								<li><span>15</span></li>
								<li><span>16</span></li>
								<li><span>17</span></li>
								<li><span>18</span></li>
								<li><span>19</span></li>
								<li><span>20</span></li>
								<li><span>21</span></li>
								<li class="current"><span>22</span></li>
								<li><span>23</span></li>
								<li><span>24</span></li>
								<li><span>25</span></li>
								<li><span>26</span></li>
								<li><span>27</span></li>
								<li><span>28</span></li>
								<li><span>29</span></li>
								<li><span>30</span></li>
							</ul>
							<div class="month"><span>Сентября</span></div>
						</div>
						<div class="left-arrow"></div>
						<div class="right-arrow"></div>
					</div>

					<div class="winners__calendar-list">
						<ul>
							<li>
								<div class="winner-item">
									<a href="#">
										<div class="photo"><img src="img/calendar/angelina.png" alt=""/></div>
										<div class="name"><span>Ольга Иванова</span></div>
									</a>

									<div class="hide-check" data-width="300" data-height="200">
										<div class="check-block">
											<p class="check-city abs">Москва</p>

											<p class="check-address abs">Толбухина ул., д. 9</p>

											<p class="terminal abs">Терминал
												№XX<br>XXXXXXXXXXXXX<br><strong>ЧЕК:&nbsp;</strong><strong
														class="check-number">13.2848.173</strong></p>

											<p class="check-date abs"><br><strong class="buy-date">28.06.2016
												17:26</strong><br>XXXXXXXXXXXXX<br><strong class="card-number">XXXXXXXXX0114</strong>
											</p>
										</div>
									</div>
								</div>
							</li>
							<li>
								<div class="winner-item">
									<a href="#">
										<div class="photo"><img src="img/calendar/angelina.png" alt=""/></div>
										<div class="name"><span>Ольга Иванова</span></div>
									</a>

									<div class="hide-check" data-width="300" data-height="200">
										<div class="check-block">
											<p class="check-city abs">Москва</p>

											<p class="check-address abs">Толбухина ул., д. 9</p>

											<p class="terminal abs">Терминал
												№XX<br>XXXXXXXXXXXXX<br><strong>ЧЕК:&nbsp;</strong><strong
														class="check-number">13.2848.173</strong></p>

											<p class="check-date abs"><br><strong class="buy-date">28.06.2016
												17:26</strong><br>XXXXXXXXXXXXX<br><strong class="card-number">XXXXXXXXX0114</strong>
											</p>
										</div>
									</div>
								</div>
							</li>
							<li>
								<div class="winner-item">
									<a href="#">
										<div class="photo"><img src="img/calendar/angelina.png" alt=""/></div>
										<div class="name"><span>Ольга Иванова</span></div>
									</a>

									<div class="hide-check" data-width="300" data-height="200">
										<div class="check-block">
											<p class="check-city abs">Москва</p>

											<p class="check-address abs">Толбухина ул., д. 9</p>

											<p class="terminal abs">Терминал
												№XX<br>XXXXXXXXXXXXX<br><strong>ЧЕК:&nbsp;</strong><strong
														class="check-number">13.2848.173</strong></p>

											<p class="check-date abs"><br><strong class="buy-date">28.06.2016
												17:26</strong><br>XXXXXXXXXXXXX<br><strong class="card-number">XXXXXXXXX0114</strong>
											</p>
										</div>
									</div>
								</div>
							</li>
							<li>
								<div class="winner-item">
									<a href="#">
										<div class="photo"><img src="img/calendar/angelina.png" alt=""/></div>
										<div class="name"><span>Ольга Иванова</span></div>
									</a>

									<div class="hide-check" data-width="300" data-height="200">
										<div class="check-block">
											<p class="check-city abs">Москва</p>

											<p class="check-address abs">Толбухина ул., д. 9</p>

											<p class="terminal abs">Терминал
												№XX<br>XXXXXXXXXXXXX<br><strong>ЧЕК:&nbsp;</strong><strong
														class="check-number">13.2848.173</strong></p>

											<p class="check-date abs"><br><strong class="buy-date">28.06.2016
												17:26</strong><br>XXXXXXXXXXXXX<br><strong class="card-number">XXXXXXXXX0114</strong>
											</p>
										</div>
									</div>
								</div>
							</li>
							<li>
								<div class="winner-item">
									<a href="#">
										<div class="photo"><img src="img/calendar/angelina.png" alt=""/></div>
										<div class="name"><span>Ольга Иванова</span></div>
									</a>

									<div class="hide-check" data-width="300" data-height="200">
										<div class="check-block">
											<p class="check-city abs">Москва</p>

											<p class="check-address abs">Толбухина ул., д. 9</p>

											<p class="terminal abs">Терминал
												№XX<br>XXXXXXXXXXXXX<br><strong>ЧЕК:&nbsp;</strong><strong
														class="check-number">13.2848.173</strong></p>

											<p class="check-date abs"><br><strong class="buy-date">28.06.2016
												17:26</strong><br>XXXXXXXXXXXXX<br><strong class="card-number">XXXXXXXXX0114</strong>
											</p>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>

            
                <img src="img/decor/winners/1.png" alt="" class="decors decor-1">
                <img src="img/decor/winners/2.png" alt="" class="decors decor-2">
                <img src="img/decor/winners/3.png" alt="" class="decors decor-3">
                <img src="img/decor/winners/4.png" alt="" class="decors decor-4">
                <img src="img/decor/winners/5.png" alt="" class="decors decor-5">
                <img src="img/decor/winners/6.png" alt="" class="decors decor-6">
                <img src="img/decor/winners/7.png" alt="" class="decors decor-7">
                <img src="img/decor/winners/8.png" alt="" class="decors decor-8">
                <img src="img/decor/winners/9.png" alt="" class="decors decor-9">
            
		</div>

		<div class="map-block">
			<div class="spiral_circle_parallax parallax-style skrollable skrollable-between" data-0="background-position:0px 4000px;" data-100000="background-position:0px -50000px;"></div>
			<div class="infinity_circle_parallax parallax-style skrollable skrollable-between" data-0="background-position:0px 4600px;" data-100000="background-position:0px -50000px;"></div>

			<div class="container more centred">
				<div class="map-block__title">
					<h2>Спеши в ближайшие магазины «Подружка»</h2>
				</div>

				<div class="shop-view">
					<div class="map__tabs">
						<form class="shop-filter-i" action="http://www.podrygka.ru/shoplist/" data-tab="map" data-lat="55.751094005949" data-lon="37.623520507812" data-zoom="10">
							<input id="city-1" type="radio" name="tabs" data-lat="55.751094005949" data-lon="37.623520507812" checked="">
							<label for="city-1" class="label-1">Москва</label>
							<input id="city-2" data-lat="59.9391" data-lon="30.3159" type="radio" name="tabs">
							<label for="city-2" class="label-2">Санкт-Петербург</label>
						</form>
					</div>
					<div class="map__maps_container">
						<div class="podrygka-store__map-content-wrap">
							<div id="content-1" class="shop-tab-view">

								<div class="map" data-panel="map" data-ajax="Y" style="display: block;">
									<div id="shops" class="shop-map">
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
				<div class="map__to_catalog">
					<a class="catalog_btn" href="http://www.podrygka.ru/catalog/">
					</a>
				</div>

			</div>
            
                <img src="img/decor/map/1.png" alt="" class="decors decor-1">
                <img src="img/decor/map/2.png" alt="" class="decors decor-2">
                <img src="img/decor/map/3.png" alt="" class="decors decor-3">
                <img src="img/decor/map/4.png" alt="" class="decors decor-4">
                <img src="img/decor/map/5.png" alt="" class="decors decor-5">
            
		</div>
	</main>

	<div class="modal-windows-container">



		<div id="simple-event" class="modal">
			<div class="modal-content">
				<div class="event-date">
					<span>02.09.16</span>
				</div>

				<div class="event-list">
					<div class="event">
						<div class="brand">
							<img src="img/modal-maxfactorr.png" alt=""/>
						</div>
						<div class="information">
							<a href="http://www.podrygka.ru/shoplist/115/">
								г.Москва, Старый гай ул., д.9
							</a>
						</div>
					</div>

					<div class="event">
						<div class="brand">
							<img src="img/modal-sleek.png" alt=""/>
						</div>
						<div class="information">
							<a href="http://www.podrygka.ru/shoplist/172/">
								г.Москва, ул. Бирюлёвская, 51к1
							</a>
						</div>
					</div>
				</div>
			</div>
        </div>



        <div id="event1" class="modal">
            <div class="modal-content">
                <div class="event-date">
                    <span>08.09.16</span>
                </div>

                <div class="event-list">
                    <div class="event one-adress">
                        <div class="brand">
                            <img src="img/partners/orly.png" alt=""/>
                        </div>
                        <div class="information">
                            <a href="http://www.podrygka.ru/shoplist/103/">
                                г.Москва, Каргопольская ул., д. 14, корп.1
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div id="event2" class="modal">
            <div class="modal-content">
                <div class="event-date">
                    <span>09.09.16</span>
                </div>

                <div class="event-list">
                    <div class="event one-adress">
                        <div class="brand">
                            <img src="img/modal-maxfactorr.png" alt=""/>
                        </div>
                        <div class="information">
                            <a href="http://www.podrygka.ru/shoplist/103/">
                                г.Москва, Каргопольская ул., д. 14, корп.1
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>



		<div id="event3" class="modal difficult">
			<div class="modal-content">
				<div class="event-date">
					<span>10.09.16</span>
				</div>

				<div class="event-list">
					<div class="event one-adress">
						<div class="brand">
							<img src="img/modal-artdeco.png" alt=""/>
						</div>
						<div class="information">
							<a href="http://www.podrygka.ru/shoplist/126/">
								Москва, ул.Авиаконструктора Миля,7 (115-1)
							</a>
						</div>
					</div>

					<div class="event one-adress">
						<div class="brand">
							<img src="img/modal-viviene.png" alt=""/>
						</div>
						<div class="information">
							<a href="http://www.podrygka.ru/shoplist/291/">
								г.Москва, Новокосинская ул., д. 47
							</a>
						</div>
					</div>
					<div class="event">
						<div class="brand">
							<img src="img/modal-artdeco.png" alt=""/>
						</div>
						<div class="information">
							<a href="http://www.podrygka.ru/shoplist/295/">
								Москва, ул.Большая Черемушкинская 1 (243-1)
							</a>
						</div>
					</div>
				</div>
			</div>
        </div>



		<div id="event4" class="modal difficult">
			<div class="modal-content">
				<div class="event-date">
					<span>15.09.16</span>
				</div>

				<div class="event-list">
					<div class="event one-adress">
						<div class="brand">
							<img src="img/modal-maxfactorr.png" alt=""/>
						</div>
						<div class="information">
							<a href="http://www.podrygka.ru/shoplist/115/">
								Москва, Старый гай ул., д. 9
							</a>
						</div>
					</div>

					<div class="event one-adress">
						<div class="brand">
							<img src="img/modal-artdeco.png" alt=""/>
						</div>
						<div class="information">
							<a href="http://www.podrygka.ru/shoplist/131/">
								Москва, ул.Первомайская, 87/1 (120-1)
							</a>
						</div>
					</div>
					<div class="event">
						<div class="brand">
							<img src="img/partners/orly.png" alt=""/>
						</div>
						<div class="information">
							<a href="http://www.podrygka.ru/shoplist/291/">
								Москва, Новокосинская ул., д. 47
							</a>
						</div>
					</div>
				</div>
			</div>
        </div>



		<div id="event5" class="modal difficult">
			<div class="modal-content">
				<div class="event-date">
					<span>17.09.16</span>
				</div>

				<div class="event-list">
					<div class="event one-adress">
						<div class="brand">
							<img src="img/modal-artdeco.png" alt=""/>
						</div>
						<div class="information">
							<a href="http://www.podrygka.ru/shoplist/188/">
								Москва, ул.Шолохова 15А (170-1)
							</a>
						</div>
					</div>

					<div class="event one-adress">
						<div class="brand">
							<img src="img/modal-viviene.png" alt=""/>
						</div>
						<div class="information">
							<a href="http://www.podrygka.ru/shoplist/208/">
								Химки, Юбилейный проспект 40 
							</a>
						</div>
					</div>
					<div class="event">
						<div class="brand">
							<img src="img/modal-artdeco.png" alt=""/>
						</div>
						<div class="information">
							<a href="http://www.podrygka.ru/shoplist/212/">
								МО, г.Подольск, Октябрьский пр-т, 9В (188-1)
							</a>
						</div>
					</div>
				</div>
			</div>
        </div>


		<div id="event6" class="modal difficult">
			<div class="modal-content">
				<div class="event-date">
					<span>22.09.16</span>
				</div>

				<div class="event-list">
					<div class="event one-adress">
						<div class="brand">
							<img src="img/modal-artdeco.png" alt=""/>
						</div>
						<div class="information">
							<a href="http://www.podrygka.ru/shoplist/139/">
								Москва, ул.Шаболовка 10к2 (127-1)
							</a>
						</div>
					</div>

					<div class="event one-adress">
						<div class="brand">
							<img src="img/divage.png" alt=""/>
						</div>
						<div class="information">
							<a href="http://www.podrygka.ru/shoplist/34/">
								Москва, 3-я Владимирская ул., д. 24Г
							</a>
						</div>
					</div>
					<div class="event">
						<div class="brand">
							<img src="img/partners/orly.png" alt=""/>
						</div>
						<div class="information">
							<a href="http://www.podrygka.ru/shoplist/224/">
								Москва, Юных Ленинцев ул., д. 73 к1
							</a>
						</div>
					</div>
				</div>
			</div>
        </div>




		<div id="event7" class="modal difficult">
			<div class="modal-content">
				<div class="event-date">
					<span>23.09.16</span>
				</div>

				<div class="event-list">
					<div class="event one-adress">
						<div class="brand">
							<img src="img/modal-maxfactorr.png" alt=""/>
						</div>
						<div class="information">
							<a href="http://www.podrygka.ru/shoplist/34/">
								Москва, 3-я Владимирская ул., д. 24Г
							</a>
						</div>
					</div>

					<div class="event one-adress">
						<div class="brand">
							<img src="img/modal-artdeco.png" alt=""/>
						</div>
						<div class="information">
							<a href="http://www.podrygka.ru/shoplist/167/">
								МО, г. Лобня, ул. Ленина , 9А (153-1)
							</a>
						</div>
					</div>
					<div class="event one-adress">
						<div class="brand">
							<img src="img/modal-artdeco.png" alt=""/>
						</div>
						<div class="information">
							<a href="http://www.podrygka.ru/shoplist/192/">
								МО, г.Железнодорожный, ул. Советская, 1Е (173-1)
							</a>
						</div>
					</div>
					<div class="event one-adress">
						<div class="brand">
							<img src="img/modal-viviene.png" alt=""/>
						</div>
						<div class="information">
							<a href="#">
								г.Серпухов, ул.Ворошилова, д. 169-171.
							</a>
						</div>
					</div>
				</div>
			</div>
        </div>



        <div id="event8" class="modal">
            <div class="modal-content">
                <div class="event-date">
                    <span>24.09.16</span>
                </div>

                <div class="event-list">
                    <div class="event one-adress">
                        <div class="brand">
                            <img src="img/modal-artdeco.png" alt=""/>
                        </div>
                        <div class="information">
                            <a href="http://www.podrygka.ru/shoplist/160/">
                                МО, г.Подольск, ул. Советская 2/1 (148-1)
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>



		<div id="event9" class="modal difficult">
			<div class="modal-content">
				<div class="event-date">
					<span>29.09.16</span>
				</div>

				<div class="event-list">
					<div class="event one-adress">
						<div class="brand">
							<img src="img/modal-artdeco.png" alt=""/>
						</div>
						<div class="information">
							<a href="http://www.podrygka.ru/shoplist/113/">
								Москва, ул.Елецкая, 15 (105-1)
							</a>
						</div>
					</div>

					<div class="event one-adress">
						<div class="brand">
							<img src="img/partners/orly.png" alt=""/>
						</div>
						<div class="information">
							<a href="http://www.podrygka.ru/shoplist/115/">
								Москва, Старый гай ул., д. 9
							</a>
						</div>
					</div>
					<div class="event">
						<div class="brand">
							<img src="img/divage.png" alt=""/>
						</div>
						<div class="information">
							<a href="http://www.podrygka.ru/shoplist/224/">
								Москва, Юных Ленинцев ул., д. 73 к1
							</a>
						</div>
					</div>
				</div>
			</div>
        </div>



		<div id="event10" class="modal">
			<div class="modal-content">
				<div class="event-date">
					<span>30.09.16</span>
				</div>

				<div class="event-list">
					<div class="event">
						<div class="brand">
							<img src="img/divage.png" alt=""/>
						</div>
						<div class="information">
							<a href="http://www.podrygka.ru/shoplist/103/">
								г.Москва, Каргопольская ул., д. 14, корп.1
							</a>
						</div>
					</div>

					<div class="event">
						<div class="brand">
							<img src="img/modal-sleek.png" alt=""/>
						</div>
						<div class="information">
							<a href="#">
								г.Москва, Кустанайская, 6 (ТЦ Столица)
							</a>
						</div>
					</div>
				</div>
            </div>
		</div>

	</div>
	<!-- Footer scripts -->
	<!--[if lt IE 9]>
	<script src="libs/html5shiv/es5-shim.min.js"></script>
	<script src="libs/html5shiv/html5shiv.min.js"></script>
	<script src="libs/html5shiv/html5shiv-printshiv.min.js"></script>
	<script src="libs/respond/respond.min.js"></script>
	<![endif]-->
	<script src="libs/jquery/jquery-1.11.1.min.js"></script>
	<script src="libs/modernizr/modernizr-custom.js"></script>

	<script src="libs/fancybox/jquery.fancybox.pack.js"></script>

	<script src="libs/countdown/jquery.plugin.js"></script>
	<script src="libs/countdown/jquery.countdown.js"></script>

	<script src="libs/parallax/jquery.parallax.min.js"></script>

	<script src="libs/scroll/skrollr.min.js"></script>
	<script src="libs/Swiper/dist/js/swiper.jquery.min.js"></script>

	<script src="libs/map/on_ymap.js"></script>

	<script src="js/common.js"></script>
	<!-- /Footer scripts -->

	<!-- Yandex.Metrika counter --><!-- /Yandex.Metrika counter -->
	<!-- Google Analytics counter --><!-- /Google Analytics counter -->
</body>
</html>